All:
	@echo "Esse projeto possui dois executaveis, escolha qual deve ser compilado..."
	@echo "make ONE para compilar o algoritmo de um bit..."
	@echo "make TWO para compilar o algoritmo de dois bits"
ONE:
	@echo "Compilando o preditor de um bit..."
	@g++ -Wall -std=c++11 src/ONE.cpp -I include -o bin/ONE_bit
TWO:
	@echo "Compilando o preditor de dois bits..."
	@g++ -std=c++11 src/two.cpp -I include -o bin/TWO_bits
clean:
	@echo "Limpando..."
	@rm -f bin/*bit*
	@rm -f bin/*SAIDA*
