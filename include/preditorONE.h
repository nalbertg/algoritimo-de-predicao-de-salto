#ifndef _PREDITOR_
#define _PREDITOR_
#include <iostream>
#include <sstream>

const size_t N_SALTOS = 4; // configurado para funcionar com 4 níveis de correlação 

class Preditor {

public:
Preditor(); // este construtor seta os valores
~Preditor() {/*vazio*/}

void processar(void);
void ler_arquivo(std::string nome_arquivo); // ler o arquivo de entrada e formata na matriz saltos
std::string saida(); // retorna a saida quando tiver terminado de processar

private:
 int interacoes;
 bool the_bit = true; // o preditor começa como tomado
 int **saltos;
 int **resultados; // guarda se ouve acerto ou erro naquela predição

 int *extrair_correlacao(int); // extrai a correlação em um vetor que contem os saltos correlacionados
 bool checar_correlacao(int, int); // retorna se existe alguma correlação e se deve testar o salto
 bool bit(int); // preditor de 1 bit
 int tokenizar(std::string nome); // converte o arquivo de entrada em uma matriz melhor formatada
 int extrair_saltos(std::string bruto, int *&saltos); // RECEBE UMA STRING E RETORNA UM VETOR CONTENDO OS SALTOS COMO 1 PARA T E 0 PARA N
};
#include "preditorONE.cpp" // criando ligação entre a implementação e a classe
#endif
