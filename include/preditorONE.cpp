#include "preditorONE.h"

Preditor::Preditor() {
  this->resultados = new int*[N_SALTOS]; // suporta todos os saltos que são N_SALTOS no máximo
}

void Preditor::processar(void) {
 for(size_t i = 0; i < N_SALTOS; i++) {
  this->the_bit  = true; // A predição começa com tomada
  for(int j = 1; j < this->interacoes;j++) { // loop principal que percorre as interações
     if(this->checar_correlacao(i, j)) { // se a correlação diz que o salto exterior não foi tomado
     	this->resultados[i][j] = this->bit(this->saltos[i][j]); // salva o resultado dessa predição para a saida
     }
     else { // se o salto não deve ser tomado por conta de sua correlação...
        this->resultados[i][j] = -1; // -1 significa que esse salto não foi testado
     }
  }
 }

}

bool Preditor::checar_correlacao(int i, int j) {
   if(this->saltos[i][0] == 0) return true; // não tem correlação com ninguem
   if(this->saltos[i][0] < 10) { // nesse caso só tem correlação com um outro salto
     if(this->saltos[this->saltos[i][0]-1][j]) return false; // o salto com o qual esse tem correlação foi tomado 
     else return true; // ou não foi tomado
   }
   else { // tem correlação com mais de um salto
   	int *correlacts = extrair_correlacao(this->saltos[i][0]);
     for(int x = 1; x <= correlacts[0]; x++) // percorre todas as correlações
       if(this->saltos[correlacts[x]-1][j]) return false; // se em alguma correlação ouver impedimento retorna false

     delete [] correlacts;
   	 return true;
   }
}

int *Preditor::extrair_correlacao(int relact) {
	int N = 1; // guarda o numero de correlações
	int casa;
	for(casa = 10; casa <= relact; casa *= 10)
	  N++; // descobrindo quantas correlações ele possui
	  casa /= 10; // agora tenho o valor correspondente a quantidade de casas que o relação possui

	int *retorno = new int[N+1];
	for(int x = N; x >= 0; x--) // zerando vetor
	  retorno[x] = 0;
	  
	for(int x = 1;N > 0;N--) {
	 if(casa < 10) { // caso especial que deve ser executado diferente
		 retorno[x] = relact;
		 retorno[0]++;
		 return retorno;
	 }
	 else { // caso básico em que ainda é nescessário extrair a correlação
		 while(relact > casa){
	      relact -= casa;
	      retorno[x]++; // a quantidade de loops corresponde a o valor daquela casa 
         }
     }
     casa /= 10;
     retorno[0]++;
     x++;
  }
  return retorno;
}


bool Preditor::bit(int x) {
   if(the_bit) { // vai tomar o salto
   	if(x == 0) { // se errou na predição
   		the_bit = false; // troca a decisão
   		return true; // mas retorna a predição que tinha
   	}
   	else
   		return true; // acertou
   }
   else { // não vai tomar o salto
    if(x == 1) { // se errou na predição
   		the_bit = true; // troca a decisão
   		return false; // mas retorna a predição que tinha
   	}
   	else
   		return false; // acertou
   }
}

void Preditor::ler_arquivo(std::string nome_arquivo) {
  this->interacoes = tokenizar(nome_arquivo); // lendo o arquivo e quardando quantas iterações ele possui
    for(size_t x = 0;x < N_SALTOS;x++)
     this->resultados[x] = new int[this->interacoes];
}

/*
formato de entrada das funçoes abaixo "STRING"
-   TNTTN
1   TNTTN
1   TNTTN
2/3 TNTTN
 formato de saida das funções a baixo "MATRIZ DE INTEIROS"
[0]  [1] [0] [1] [1] [0]
[1]  [1] [0] [1] [1] [0]
[1]  [1] [0] [1] [1] [0]
[23] [1] [0] [1] [1] [0]
*/
int Preditor::tokenizar(std::string nome) {
 std::string linha; // armazena a linha completa do arquivo

 std::ifstream arquivo; // instanciando um ponteiro pro arquivo
 arquivo.open(nome); // abrindo o arquivo

 if(!(arquivo.is_open() && arquivo.good()))  { // verificar se abriu e se está "usavel"
   std::cout << "\nFalha !!!  Não foi possivel abrir o arquivo \"" << nome << "\"\n\n";
   exit(0); // saida do programa por erro fatal
 }
this->saltos = new int*[N_SALTOS]; // quatro níveis de correlação
size_t x = 0; // essa vriavel coloca os saltos em suas posições na matriz
int quantos; // guarda quantas interaçãoes esses saltos vão ter
while(!arquivo.fail() && x < N_SALTOS) { // esse loop vai percorrer linha a linha do arquivo lendo até N_SALTOS
 std::getline(arquivo, linha); // lendo a linha e guardando na string linha
 quantos = extrair_saltos(linha, this->saltos[x]); // extraindo todos os saltos encontratos na linha e armazenando na matriz
 x++;
}

arquivo.close(); // fechando apos terminar de ler
return quantos-1; // menos um retornará o resultado correto
}

int Preditor::extrair_saltos(std::string bruto, int *&saltos) {
  saltos = new int[bruto.size()]; // alocando espaço para todas as interações
  saltos[0] = 0; // esse elemento representa a correlação do saltos
  int x = 1; // variável de controle para o vetor de saltos
  bool lendo_correlacao = true; // esse booleano controla se a correlação está sendo lida nesse momento
   for(int i = 0; bruto[i] != '\0'; i++)  { // percorrer todos os caracteres
    if(lendo_correlacao) {//  verificar se o o salto tem correlação
      if(bruto[i] == '-') { // caso não aja correlação de salto
        saltos[0] = 0; // não tem correlação com ninguem
      }
      else {
       if(bruto[i] == ' ')
        lendo_correlacao = false; // terminou de ler correlação e vai começar com as interações
       else {
        if(bruto[i] == '/') saltos[0] *= 10; // abrindo espaço para por a nova correlação
        else saltos[0] += bruto[i] - '0'; // convertendo de caractere para inteiro
       }
      }
    }
    else { // caso o caracter atual NÃO seja um numero porem o ultimo caracter lido era um numero
      if(bruto[i] == 'T')
        saltos[x] = 1;
      else
        saltos[x] = 0;
      x++;
    }
  }
  return x+1;
}

std::string Preditor::saida() {
  std::stringstream saida; // saida inicialmente vazia
  int acertos;
  int total;
  double percentagem;

  size_t Nespacos = (this->interacoes-8)*3;
  std::string espacos = "";
  for(;Nespacos > 0; Nespacos--)
    espacos += " ";

  saida << "Salto  " << "Predição por iteração  " << espacos << "Taxa de acerto\n\n";
    for(int x = 0;this->saltos[x] != nullptr;x++) { // debug
      saida << "   " << x+1 << "   ";
    for(int y = 1; y < this->interacoes; y++) { // loop que grava as predições
     switch(resultados[x][y]) {
       case 1: // se o resultado for tomar
       saida << " T ";
       break;
       case 0: // se for não tomar
       saida << " N ";
       break;
       default: // se a correlação impede o teste daquele salto
       saida << " - ";
       break;
     }
    }
    saida << "\n       "; // apenas para formatar melhor
    acertos = 0;
    total = this->interacoes-1;
    for(int y = 1; y < this->interacoes; y++) {
     if(resultados[x][y] == saltos[x][y]){
       saida << " A ";
       acertos++;
     }
     else if(resultados[x][y] == -1){
       saida << " - ";
       total--;
     }
     else {
       saida << " E ";
     }
    }
    percentagem = (acertos*100)/total;
    saida << "   " << acertos << "/" << total << " = " << percentagem << "%";
    saida << "\n\n";
  }
  return saida.str();
}
