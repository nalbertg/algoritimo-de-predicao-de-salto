#include <iostream>
#include <fstream>
#include <string>

#include "preditorONE.h"

int extrair_saltos(std::string string, int *&numeros);
int tokenizar(int **&matriz, std::string nome);

int main(int argc, char * argv[]) {
    std::string nome;
    if ( argc > 1 ) {
        nome =  argv[1]; // recebe o nome do arquivo por parametro
    }
    else {
        std::cout << "\nErro !!! O local do arquivo de entrada não foi passado por parâmetro\n\n";
        return false;
    }

    Preditor preditor;
    
    preditor.ler_arquivo(nome);
    preditor.processar();
    std::string saida(preditor.saida());
    std::cout << "\n" << saida;
   
     std::ofstream arquivo; // instanciando um ponteiro pro arquivo
     arquivo.open("SAIDA_OneBit.txt"); // abrindo o arquivo
     arquivo << saida; // gravando a saida
     arquivo.close();
     std::cout << "\nArquivo de saida gerado >>> SAIDA_OneBit.txt\n";

   return 0;
}
